package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by kianutamaradianur on 3/1/18.
 */
public class Cucumber extends Food {
    Food food;

    public Cucumber(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding cucumber";
    }

    @Override
    public double cost() {
        return 0.4 + food.cost();
    }
}
