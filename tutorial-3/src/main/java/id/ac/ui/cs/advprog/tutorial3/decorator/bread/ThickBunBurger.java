package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by kianutamaradianur on 3/1/18.
 */
public class ThickBunBurger extends Food {
    public ThickBunBurger() {
        description = "Thick Bun Burger";
    }

    @Override
    public double cost() {
        return 2.5;
    }
}
